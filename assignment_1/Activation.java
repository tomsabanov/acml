import java.util.function.DoubleFunction;

public class Activation extends Layer{
    // Activation layer

    private DoubleFunction<Double> activation;
    private DoubleFunction<Double> prime;

    public Activation(DoubleFunction<Double> activation, DoubleFunction<Double> prime){
        // we need to provide the activation function and its derivative
        this.activation = activation; 
        this.prime = prime;
    }

    @Override
    public boolean isActivation(){
        return true;
    }

    @Override
    public Matrix forward_propagate(Matrix input){
        this.input = input;
        return input.apply(this.activation);
    }

    @Override
    public Matrix backward_propagate(Matrix output_grad, double lr, double lambda){
        return output_grad.multiplyByElement(this.input.apply(this.prime));
    }
    
    @Override
    public String toString(){
        return "No weights and biases - this is an activation layer!";
    }
}
