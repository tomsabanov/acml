import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class Network {
    private Layer[] layers;

    private double[][] input;
    private double[][] output;

    private double[] errors = null;


    private int epochs = 0;
    private double learning_rate = 0.0;
    private double lambda = 0.0; // weight_decay



    public Network(Layer[] layers, double[][] input, double[][] output){
        this.layers = layers;
        this.input = input;
        this.output = output;
    }

    public double mse(Matrix actual, Matrix predicted){
        return actual.subtract(predicted).power(2).sum() / actual.getHeight();
    }
    public Matrix mse_prime(Matrix actual, Matrix predicted){
        return predicted.subtract(actual).multiply((double)2.0/((double)actual.getHeight()));
    }

    public double train(int epochs, double lr, double lambda){
        this.errors = new double[epochs];
        this.epochs = epochs;
        this.learning_rate = lr;
        this.lambda = lambda;

        System.out.println("Started training the neural net!");
        System.out.println("Epochs: " + epochs);
        System.out.println("Learning rate: " + lr);        
        System.out.println("Weight decay: " + lambda);        
        System.out.println("========================================================");
        
        for(int i = 0; i<epochs; i++){
            double err = 0;

            for(int j = 0; j<input.length; j++){
                double[][] x_vec = {input[j]};
                Matrix x = new Matrix(x_vec).transpose();

                double[][] y_vec = {output[j]};
                Matrix y = new Matrix(y_vec).transpose();

                
                // Forward propagation
                Matrix out = x;
                for(int k = 0; k<layers.length; k++){
                    out = layers[k].forward_propagate(out);
                }


                err += mse(y, out);

                // Backward propagation
                Matrix g = this.mse_prime(y, out);
                for(int k = layers.length-1; k>=0; k--){
                    g = layers[k].backward_propagate(g, lr, lambda);
                }

            }

            err /= input.length;
            
            // Save the errors
            this.errors[i] = err;

            System.out.printf("Epoch %d/%d, error=%f\n", i+1, epochs, err );
            System.out.println("----------------------------");
        }

        return 0.0;
    }

    public void predict(double[] input){
        double[][] x_vec = {input};
        Matrix x = new Matrix(x_vec).transpose();


        Matrix out = x;
        for(int k = 0; k<layers.length; k++){
            out = layers[k].forward_propagate(out);
        }
        System.out.println(out.toString());
    }


    public void saveWeights(String filename){
        // Save the weights of the network to a file

        try{

            StringBuilder builder = new StringBuilder();
            for(int k = 0; k<layers.length; k++){
                if(layers[k].isActivation()) continue;

                double[][] w = layers[k].getWeights();
                for(int i = 0; i<w.length; i++){
                    int j = 0;
                    for(; j<w[i].length-1; j++){
                        builder.append(w[i][j] + ",");
                    }
                    builder.append(w[i][j] + "\n");
                }

                // delimiter for loading the weights back to a model
                builder.append("============\n"); 
            }

            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            writer.write(builder.toString());
            writer.close();

        }
        catch(IOException e){
            // Handle exception -- On a later date, 
            // the entire code could use a bit of error handling and
            // proper logging
        }

    }

    public void saveNetwork(String filename, int duplicate){
        // We save the results of the network, not the structure itself
        // -> this will come at a later date
        
        // We save the number of iterations, 
        // learning rate, lambda, errors
        // input length and duplicates

        StringBuilder builder = new StringBuilder();
        builder.append("Iterations: " + this.epochs + "\n");
        builder.append("Learning rate: " + this.learning_rate + "\n");
        builder.append("Weight decay: " + this.lambda + "\n");
        builder.append("Number of inputs: " + this.input.length + "\n");
        builder.append("Number of duplicates: " + duplicate + "\n");
        builder.append("Final loss error: " + this.errors[this.epochs-1] + "\n");
        builder.append("Errors: " + Arrays.toString(this.errors) + "\n");

        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            writer.write(builder.toString());
            writer.close();            
        }
        catch(IOException io){
            // ignore
        }

    }




}
