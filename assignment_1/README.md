Compile every .class file (javac *.java)

run the Main class (java Main num_iterations learning_rate weight_decay duplicates weight_file neural_net_file)

Num_iterations = number of iterations the neural net will train for
learning_rate = the learning rate used in the neural net
weight_decay = the weight decay (lambda) being used in the neural net
duplicates = the number of original duplicated samples (we have 8 different training samples, 
        if duplicate = 100, then the entire training dataset will have 800 samples)

Weights_file = path to weights file to save to
neural_net file = path to neural net info file to save info


In the folders experiments1,2,3 and 4 are all of the experiments. 
In experiments1 and experiments3 sigmoid function is used, while
in experiments2 and experiments4 tanh function is used.