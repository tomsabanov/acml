import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.function.DoubleFunction;

import javax.swing.text.html.HTMLDocument.Iterator;

public class Main {
    
    // Collections of inputs and outputs for this assignment
    static double[][] input = {
        {1,0,0,0,0,0,0,0}, 
        {0,1,0,0,0,0,0,0}, 
        {0,0,1,0,0,0,0,0}, 
        {0,0,0,1,0,0,0,0},
        {0,0,0,0,1,0,0,0},
        {0,0,0,0,0,1,0,0},
        {0,0,0,0,0,0,1,0},
        {0,0,0,0,0,0,0,1}
    };
    static double[][] output = {
        {1,0,0,0,0,0,0,0}, 
        {0,1,0,0,0,0,0,0}, 
        {0,0,1,0,0,0,0,0}, 
        {0,0,0,1,0,0,0,0},
        {0,0,0,0,1,0,0,0},
        {0,0,0,0,0,1,0,0},
        {0,0,0,0,0,0,1,0},
        {0,0,0,0,0,0,0,1}
    };


    public static void main(String[] args){

        //assignment();

        generate_experiments();

    }

    public static void generate_experiments(){
        // Sigmoid activation functions
        DoubleFunction<Double> sigm = x -> 1 / (1 + Math.exp(-x));
        DoubleFunction<Double> sigm_prime = x -> Math.exp(-x) / ((1 + Math.exp(-x)) * (1 + Math.exp(-x)));
        
        DoubleFunction<Double> tanh = x -> Math.tanh(x);
        DoubleFunction<Double> tanh_prime = x -> 1 - Math.tanh(x) * Math.tanh(x);

        DoubleFunction<Double>[] act = new DoubleFunction[4];
        act[0] = sigm;
        act[1] = sigm_prime;
        act[2] = tanh;
        act[3] = tanh_prime;
 
        int iterations = 10000; // fixed number of iterations

        double[] lrates = new double[]{0.1, 1, 10}; // learning rates
        double[] lambdas = new double[]{0.0,0.1,0.5, 1}; // weight decays

        int[][] shapes = new int[][]{
            {8,1},
            {8,10},
            {8,100}
        };
    
        int exp = 0;
        for(int i = 0; i<lrates.length; i++){
            double lr = lrates[i];

            for(int j = 0; j < lambdas.length; j++){
                double lambda = lambdas[j];

                    for(int m = 0; m<shapes.length; m++){
                        int[] sh = shapes[m];
                        int numInputs = sh[0];
                        int dups = sh[1];

                        String weights_file = "./experiments2/" + exp + "_weights.txt";
                        String network_file = "./experiments2/" + exp + "_network.txt";

                        test_neural_net(numInputs, dups, iterations, lr, lambda, tanh, tanh_prime, weights_file, network_file);
                        exp++;
                    }
                
            }

        }

    }

    public static void test_neural_net(
        int numOfInputs, 
        int duplicate,
        int iterations,
        double learning_rate,
        double lambda,
        DoubleFunction<Double> act,
        DoubleFunction<Double> act_prime,
        String weights_file,
        String network_file
    )
    {

        Layer[] layers = {
            new Dense(8,3),
            new Activation(act,act_prime),
            new Dense(3,8),
            new Activation(act,act_prime),
        };


        // Select random samples from input/output
        Random rng = new Random();
        int[] randoms = new int[numOfInputs];
        int sel = 0;
        while ( sel < numOfInputs){
            int next = rng.nextInt(8) + 1;
            if(contains(randoms, next)) continue;
            randoms[sel] = next;
            sel++;
        }

        double[][] in = new double[numOfInputs * duplicate][8];
        double[][] out = new double[numOfInputs * duplicate][8];
        for(int i = 0; i<numOfInputs; i++){
            for(int j = 0; j<duplicate; j++){
                in[i*duplicate + j] = input[randoms[i] - 1];
                out[i*duplicate + j] = output[randoms[i] - 1];
            }
        }
        // -------------------------------------------------

        Network net = new Network(layers, in, out);

        net.train(iterations,learning_rate,lambda);
                
        for(int i = 0; i<input.length; i++){
            net.predict(input[i]);
            break;
        }
        net.saveWeights(weights_file);
        net.saveNetwork(network_file, duplicate);

    }


    public static void assignment(){
                // sigmoid activation function
                DoubleFunction<Double> sigm = x -> 1 / (1 + Math.exp(-x));
                DoubleFunction<Double> sigm_prime = x -> Math.exp(-x) / ((1 + Math.exp(-x)) * (1 + Math.exp(-x)));
        
                Layer[] layers = {
                    new Dense(8,3),
                    new Activation(sigm,sigm_prime),
                    new Dense(3,8),
                    new Activation(sigm,sigm_prime),
                };
        
                Network net = new Network(layers, input, output);
        
                net.train(1000,1,0.5);
                
                for(int i = 0; i<input.length; i++){
                    net.predict(input[i]);
                    break;
                }

    }


    public static boolean contains(final int[] arr, final int key) {
        return Arrays.stream(arr).anyMatch(i -> i == key);
    }
}
