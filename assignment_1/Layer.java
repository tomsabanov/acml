
public abstract class Layer{

    protected Matrix input;
    protected Matrix output;

    public Layer(){}
    public Layer(int input, int output){
        this.input = new Matrix(input, 1);
        this.output = new Matrix(output, 1);
    }

    public double[][] getWeights(){
        return null;
    }

    abstract Matrix forward_propagate(Matrix input);
    abstract Matrix backward_propagate(Matrix output_grad, double learning_rate, double lambda); 
    abstract boolean isActivation();

}