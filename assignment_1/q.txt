Questions and anwsers for Q&A 1 ACML:
-----------------------------------------------------------



1. Should we use cross entropy as a cost function or squared differences?
	-> try to use squared differences, its going to be easiest



2. Do we need to handle overfitting -> NO - but we can run some experiments
  to see what happens
  
 

Implement weight decay to see what happens


Be careful about the vectorization in forward propagate and backward propagate
(order of products)


3. What lambda value should we use when doing regularization?
	-> experiment (set it to different values, 0, 1, 1000,....)
	
	
4. Does capital delta(triangle) - they are used to accumulate over all learning examples,
	when do we set them to zero? -> 
	
	
	
you update the weights of bias nodes, but you dont calculate a delta 
for them, since bias nodes only have outputs and no inputs, therefore 
they cannot change any previous weights.....