public class Dense extends Layer{
    // a.k.a fully connected layer

    private Matrix weights;
    private Matrix bias;

    public Dense(int input, int output){
        super(input,output);
        this.weights = new Matrix(output,input, -1,1);
        this.bias = new Matrix(output,1, 1);
    }

    public double[][] getWeights(){
        return this.weights.getMatrix();
    }

    @Override
    public boolean isActivation(){
        return false;
    }

    @Override
    public Matrix forward_propagate(Matrix input){
        this.input = input;
        return this.weights.multiply(input).add(this.bias); 
    }

    /*
    This is the basic backward propagation without any weight decay
    
    @Override
    public Matrix backward_propagate(Matrix grad, double lr, double lambda){
        Matrix weights_gradient = grad.multiply(this.input.transpose());
        
        this.weights = this.weights.subtract(weights_gradient.multiply(lr));
        this.bias = this.bias.subtract(grad.multiply(lr));

        return this.weights.transpose().multiply(grad);
    }
    */


    @Override
    public Matrix backward_propagate(Matrix grad, double lr, double lambda){
        Matrix weights_gradient = grad.multiply(this.input.transpose());
        this.weights = this.weights.subtract(weights_gradient.add(this.weights.multiply(lambda)).multiply(lr));
        this.bias = this.bias.subtract(grad.multiply(lr));

        return this.weights.transpose().multiply(grad);
    }


    @Override
    public String toString(){
        String s = "";
        s += "Weights: \n";
        s += weights.toString();

        s+="\n";

        s+= "Biases: \n"; 
        s+= bias.toString();

        return s;
    }
}
