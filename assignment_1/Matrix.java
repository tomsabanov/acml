import java.util.Arrays;
import java.util.function.DoubleFunction;

public class Matrix {
    private final int width;
    private final int height;

    private final double[][] m;

    public Matrix(int h, int w){
        this.width = w;
        this.height = h;

        this.m = new double[h][w];
    }

    public Matrix(int h, int w, double from, double to){
        this.width = w;
        this.height = h;

        this.m = new double[h][w];

        for(int i = 0; i<h; i++){
            for(int j = 0; j<w; j++){
                this.m[i][j] = Math.random() * (to-from) + from;
            }
        }
    }

    public Matrix(int h, int w, double val){
        this.width = w;
        this.height = h;

        this.m = new double[h][w];

        for(int i = 0; i<h; i++){
            for(int j = 0; j<w; j++){
                this.m[i][j] = val;
            }
        }        
    }

    public Matrix(double[][] t) {
        this(t.length, t[0].length);
        
        for(int i=0; i<this.getHeight(); i++) {
          for(int j=0; j<this.getWidth(); j++) {
            this.setValue(i, j, t[i][j]);
          }
        }
      }

    
    public double[][] getMatrix(){
        return this.m;
    }

    public int getHeight(){
        return this.height;
    }
    public int getWidth(){
        return this.width;
    }



    public void setValue(int row, int col, double val){
        this.m[row][col] = val;
    }
    public double getValue(int row, int col){
        return this.m[row][col];
    }


    @Override
    public String toString(){
        String s = "";
        for(int i = 0; i<this.height; i++){
            s+=Arrays.toString(this.m[i]) + "\n";
        }
        return s;
    }


    // Operations
    public double sum(){
        double sum = 0;
        for(int i = 0; i<this.height; i++){
            for(int j = 0; j<this.width; j++){
                sum += this.getValue(i, j);
            }
        }
        return sum;
    }

    public Matrix add(Matrix m2){
        // check if height and widths are equal
        if(m2.getHeight() != this.height || m2.getWidth() != this.width){
            System.out.println("Dimensions of matrices are not compatible!");
            return null;
        }

        Matrix n = new Matrix(this.height, this.width);
        for(int i = 0; i<this.height; i++){
            for(int j = 0; j<this.width; j++){
                n.setValue(i,j, m2.getValue(i,j) + this.getValue(i,j));
            }
        }

        return n;
    }

    public Matrix subtract(Matrix m2){
        // check if height and widths are equal
        if(m2.getHeight() != this.height || m2.getWidth() != this.width){
            System.out.println("Dimensions of matrices are not compatible in subtract!");
            return null;
        }

        Matrix n = new Matrix(this.height, this.width);
        for(int i = 0; i<this.height; i++){
            for(int j = 0; j<this.width; j++){
                n.setValue(i,j, this.getValue(i,j) - m2.getValue(i,j));
            }
        }

        return n;
    }

    public Matrix multiply(Matrix m2){
        // check if dimensions are compatible, 
        // we return A = this * m2
        if(this.getWidth() != m2.getHeight()){
            System.out.println("Dimensions are not compatible in multiply!");
            return null;
        }

        Matrix n = new Matrix(this.getHeight(), m2.getWidth());


        for(int i = 0; i<n.getHeight(); i++){
            for(int j = 0; j<n.getWidth(); j++){
                double sum = 0;
                for(int k = 0; k<this.getWidth(); k++){
                    sum+= this.getValue(i, k) * m2.getValue(k, j);
                }

                n.setValue(i, j, sum);
            }
        }

        return n;
    }

    public Matrix multiply(double val){
        Matrix n = new Matrix(this.getHeight(), this.getWidth());

        for(int i = 0; i<this.getHeight(); i++){
            for(int j = 0; j<this.getWidth(); j++){
                n.setValue(i,j,val * this.getValue(i,j));
            }
        }

        return n;
    }

    public Matrix multiplyByElement(Matrix m2){
        if(this.getHeight() != m2.getHeight() || this.getWidth() != m2.getWidth()){
            System.out.println("Dimensions are not compatible in multiplyByElement");
            return null;
        }
        Matrix n = new Matrix(this.getHeight(), this.getWidth());

        for(int i = 0; i<this.getHeight(); i++){
            for(int j = 0; j<this.getWidth(); j++){
                n.setValue(i,j,m2.getValue(i, j) * this.getValue(i,j));
            }
        }

        return n;
    }

    public Matrix divide(double val){
        Matrix n = new Matrix(this.getHeight(), this.getWidth());

        for(int i = 0; i<this.getHeight(); i++){
            for(int j = 0; j<this.getWidth(); j++){
                n.setValue(i,j,this.getValue(i,j)/val);
            }
        }

        return n;        
    }

    public Matrix power(double val){
        Matrix n = new Matrix(this.getHeight(), this.getWidth());

        for(int i = 0; i<this.getHeight(); i++){
            for(int j = 0; j<this.getWidth(); j++){
                n.setValue(i,j, Math.pow(this.getValue(i,j),val));
            }
        }

        return n;        
    }


    public Matrix transpose(){
        Matrix n = new Matrix(this.getWidth(), this.getHeight());

        for(int i = 0; i<this.getHeight(); i++){
            for(int j = 0; j<this.getWidth(); j++){
                n.setValue(j,i,this.getValue(i,j));
            }
        }

        return n;
    }

    public Matrix apply(DoubleFunction<Double> fnc){
        Matrix n = new Matrix(this.getHeight(), this.getWidth());

        for(int i = 0; i<this.getHeight(); i++){
            for(int j = 0; j<this.getWidth(); j++){
                n.setValue(i, j, fnc.apply(this.getValue(i,j)));
            }
        }

        return n;
    }
    
}
